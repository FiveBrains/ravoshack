<?php
include('include/top.php');
?>
		<div id="colorlib-main">
			<section class="ftco-section ftco-no-pt ftco-no-pb">
	    	<div class="container px-0">
	    		<div class="row d-flex no-gutters">
	    			<div class="col-lg-8 px-md-5 py-5">
	    				<div class="row">
	    					<h1 class="mb-3">Cyber Security in Industrial Automation</h1>
		            <p>Industrial automation and control systems (IACS) used in manufacturing and process automation are increasingly implemented within internal and external company networks. Increased data transfer between systems and networks increases susceptibility for security threats.</p>
		            <p>
		              <img src="images/cybersecurity4.jpg" alt="" class="img-fluid">
		            </p>
		            <p>On the one hand, manufacturers seek sufficient protection against manipulation and interference during product development and integration. On the other hand, system integrators need secure IT infrastructures and service processes. We provide comprehensive assessment and certification services for your IACS and components, and evaluate your defense in depth strategy in order to achieve compliance with internationally applicable safety and cyber security standards.</p>		            		           		           		            		            
		            <div class="about-author d-flex p-4 bg-light">
		              <div class="bio mr-5">
		                <img src="images/ravi.jpg" alt="Image placeholder" class="img-fluid mb-4">
		              </div>
		              <div class="desc">
		                <h3>V Ravishankar</h3>
		                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus itaque, autem necessitatibus voluptate quod mollitia delectus aut, sunt placeat nam vero culpa sapiente consectetur similique, inventore eos fugit cupiditate numquam!</p>
		              </div>
		            </div>


		            <div class="pt-8 mt-7">
		              <div class="comment-form-wrap pt-5">
		                <h3 class="mb-5">Leave a comment</h3>
		                <form action="#" class="p-3 p-md-4 bg-light" style="width: 610px;">
		                  <div class="form-group">
		                    <label for="name">Name *</label>
		                    <input type="text" class="form-control" id="name">
		                  </div>
		                  <div class="form-group">
		                    <label for="email">Email *</label>
		                    <input type="email" class="form-control" id="email">
		                  </div>
		                  <div class="form-group">
		                    <label for="website">Website</label>
		                    <input type="url" class="form-control" id="website">
		                  </div>

		                  <div class="form-group">
		                    <label for="message">Message</label>
		                    <textarea name="" id="message" cols="30" rows="10" class="form-control"></textarea>
		                  </div>
		                  <div class="form-group">
		                    <input type="submit" value="Post Comment" class="btn py-3 px-4 btn-primary">
		                  </div>

		                </form>
		              </div>
		            </div>
			    		</div><!-- END-->
			    	</div>
	    			<div class="col-lg-4 sidebar ftco-animate bg-light pt-5">	            
				<div class="sidebar-box ftco-animate">
					<h3 class="sidebar-heading">Archives</h3>
						<ul class="categories">
						 <li><a href="#">January 2020 <span>(10)</span></a></li>
						 <li><a href="#">December 2019 <span>(10)</span></a></li>
						 <li><a href="#">September 2019 <span>(6)</span></a></li>
						 <li><a href="#">August 2019 <span>(8)</span></a></li>
						 <li><a href="#">July 2019 <span>(2)</span></a></li>
						 <li><a href="#">June 2019 <span>(7)</span></a></li>
						 <li><a href="#">May 2019 <span>(5)</span></a></li>
	              </ul>
	            </div>
	            <div class="sidebar-box ftco-animate">
	              <h3 class="sidebar-heading">Popular Articles</h3>
	              <div class="block-21 mb-4 d-flex">
	                <a class="blog-img mr-4" style="background-image: url(images/cybersecurity1.jpg);"></a>
	                <div class="text">
	                  <h3 class="heading"><a href="#">Even the all-powerful Pointing has no control</a></h3>
	                  <div class="meta">
	                    <div><a href="#"><span class="icon-calendar"></span> Sept. 12, 2019</a></div>
	                    <div><a href="#"><span class="icon-person"></span> Dave Lewis</a></div>
	                    <div><a href="#"><span class="icon-chat"></span> 19</a></div>
	                  </div>
	                </div>
	              </div>
	              <div class="block-21 mb-4 d-flex">
	                <a class="blog-img mr-4" style="background-image: url(images/cybersecurity2.jpg);"></a>
	                <div class="text">
	                  <h3 class="heading"><a href="#">Even the all-powerful Pointing has no control</a></h3>
	                  <div class="meta">
	                    <div><a href="#"><span class="icon-calendar"></span> Sept. 12, 2019</a></div>
	                    <div><a href="#"><span class="icon-person"></span> Dave Lewis</a></div>
	                    <div><a href="#"><span class="icon-chat"></span> 19</a></div>
	                  </div>
	                </div>
	              </div>
	              <div class="block-21 mb-4 d-flex">
	                <a class="blog-img mr-4" style="background-image: url(images/cybersecurity3.jpg);"></a>
	                <div class="text">
	                  <h3 class="heading"><a href="#">Even the all-powerful Pointing has no control</a></h3>
	                  <div class="meta">
	                    <div><a href="#"><span class="icon-calendar"></span> Sept. 12, 2019</a></div>
	                    <div><a href="#"><span class="icon-person"></span> Dave Lewis</a></div>
	                    <div><a href="#"><span class="icon-chat"></span> 19</a></div>
	                  </div>
	                </div>
	              </div>
	            </div>
				<div class="sidebar-box pt-md-4">
	              <form action="#" class="search-form">
	                <div class="form-group">
	                  <span class="icon icon-search"></span>
	                  <input type="text" class="form-control" placeholder="Search">
	                </div>
	              </form>
	            </div>				
	          </div><!-- END COL -->
	    		</div>
	    	</div>
	    </section>
		</div><!-- END colorlib-MAIN -->
	</div><!-- END colorlib-PAGE -->

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="js/jquery.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/jquery.waypoints.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>
  <script src="js/jquery.animateNumber.min.js"></script>
  <script src="js/scrollax.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="js/google-map.js"></script>
  <script src="js/main.js"></script>
    
  </body>
</html>