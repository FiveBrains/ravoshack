<?php
include('include/top.php');
?>
		<div id="colorlib-main">
			<section class="ftco-section ftco-no-pt ftco-no-pb">
	    	<div class="container px-0">
	    		<div class="row no-gutters">
	    			<div class="col-md-12 blog-wrap">
	    				<div class="row no-gutters align-items-center">
	    					<div class="col-md-6 img js-fullheight" style="background-image: url(images/ck1.jpg);">
	    						
	    					</div>
	    					<div class="col-md-6">
	    						<div class="text p-md-5 p-4 ftco-animate">
	    							<div class="heading">
	    								<div class="meta-wrap">
												<p class="meta">
				              		<span><i class="icon-calendar mr-2"></i>Sept. 10, 2019</span>
				              	</p>
			              	</div>
		    							<h2 class="mb-5"><a href="blog.php">Six Skills You Need to Succeed in Cyber Security</a></h2>
	    							</div>
	    							<p>One reason companies can’t find the experienced cybersecurity professionals they need: there just aren’t many tech pros who have mastered not only the necessary technical abilities, but also “soft skills” (such as clear communication)—and those who have, well, they’re already employed (often with hefty salaries and benefits designed to keep them in place for the long term).</p>
	    							<div class="icon d-flex align-items-center my-5">
	    								<div class="img" style="background-image: url(images/ck1.jpg);"></div>
	    								<div class="position pl-3">
	    									<h4 class="mb-0">Dice</h4>
	    									<span><a href="https://insights.dice.com/cybersecurity-skills/" target=" ">insights.dice.com</a></span>
	    								</div>
	    							</div>
	    						</div>
	    					</div>
	    				</div>
	    			</div>
	    			<div class="col-md-12 blog-wrap">
	    				<div class="row no-gutters align-items-center">
	    					<div class="col-md-6 img js-fullheight" style="background-image: url(images/ck2.jpg);">
	    						
	    					</div>
	    					<div class="col-md-6">
	    						<div class="text p-md-5 p-4 ftco-animate">
	    							<div class="heading">
	    								<div class="meta-wrap">
												<p class="meta">
				              		<span><i class="icon-calendar mr-2"></i>Sept. 26, 2017</span>
				              	</p>
			              	</div>
		    							<h2 class="mb-5"><a href="blog.php">What is cyber security? What you need to know</a></h2>
	    							</div>
	    							<p>Cyber security is the state or process of protecting and recovering networks, devices and programs from any type of cyberattack.</p>
	    							<div class="icon d-flex align-items-center my-5">
	    								<div class="img" style="background-image: url(images/ck2.jpg);"></div>
	    								<div class="position pl-3">
	    									<h4 class="mb-0">Alison Grace Johansen</h4>
	    									<span><a href="https://us.norton.com/internetsecurity-malware-what-is-cybersecurity-what-you-need-to-know.php" target=" ">us.norton.com</a></span>
	    								</div>
	    							</div>
	    						</div>
	    					</div>
	    				</div>
	    			</div>
	    			<div class="col-md-12 blog-wrap">
	    				<div class="row no-gutters align-items-center">
	    					<div class="col-md-6 img js-fullheight" style="background-image: url(images/ck3.jpg);">
	    						
	    					</div>
	    					<div class="col-md-6">
	    						<div class="text p-md-5 p-4 ftco-animate">
	    							<div class="heading">
	    								<div class="meta-wrap">
												<p class="meta">
				              		<span><i class="icon-calendar mr-2"></i>Sept. 10, 2019</span>
				              	</p>
			              	</div>
		    							<h2 class="mb-5"><a href="blog.php">Protecting your cyber assets and critical data</a></h2>
	    							</div>
	    							<p>Cyber security has never been simple. And because attacks evolve every day as attackers become more inventive, it is critical to properly define cyber security and identify what constitutes good cyber security.</p>
	    							<div class="icon d-flex align-items-center my-5">
	    								<div class="img" style="background-image: url(images/ck3.jpg);"></div>
	    								<div class="position pl-3">
	    									<h4 class="mb-0">FireEye</h4>
	    									<span><a href="https://www.fireeye.com/current-threats/what-is-cyber-security.php" target=" ">www.fireeye.com</a></span>
	    								</div>
	    							</div>
	    						</div>
	    					</div>
	    				</div>
	    			</div>	    				    				    			
	    		</div>
	    	</div>
	    </section>
		</div><!-- END colorlib-MAIN -->
	</div><!-- END colorlib-PAGE -->

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="js/jquery.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/jquery.waypoints.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>
  <script src="js/jquery.animateNumber.min.js"></script>
  <script src="js/scrollax.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="js/google-map.js"></script>
  <script src="js/main.js"></script>
    
  </body>
</html>