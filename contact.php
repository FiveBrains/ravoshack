<?php
include('include/top.php');
?>
		<div id="colorlib-main">
			<section class="ftco-section ftco-no-pt ftco-no-pb">
	    	<div class="container px-0">
	        <div class="row no-gutters block-9">
	          <div class="col-lg-12 order-md-left">
	            <form action="send-form-email.php" id="contactform_new" method="post" class="bg-primary p-5 contact-form">				
	              <div class="form-group">
	                <input type="text" class="form-control" name="name" placeholder="Your Name">
	              </div>
	              <div class="form-group">
	                <input type="text" class="form-control" name="email" placeholder="Your Email">
	              </div>
	              
	              <div class="form-group">
	                <input type="text" class="form-control" name="message" placeholder="Your Message">
	              </div>
	              <div class="form-group">	                
					<button type="submit" name="submit" class="btn btn-white py-3 px-5">Send Message</button>
	              </div>
	            </form>	          
	          </div>

	          <!--<div class="col-lg-6 d-flex">
	          	<div class="right"><img src="images/ravi1.jpg" style="width: 572px;height: 572px;"></div>
	          </div>-->
	        </div>

	        <div class="row d-flex mb-5 px-4 px-md-4 contact-info mt-5">
				<div class="col-md-12 mb-4">
					<h2 class="h3">Contact Information</h2>
				</div>
				<div class="w-100"></div>
				<div class="col-lg-6 col-xl-4 mb-4" align="center">
					<div class="info">
						<img src="images/ravi1.jpg" style="width: 150px;height: 150px;">						
					</div>
				</div>
				<div class="col-lg-6 col-xl-4 mb-4">
				<div class="info">
					<b><p><span>Address:</span></p></b>
					<P> 1525 Boring Lane, Los Angeles, CA</p>					
				  </div>
				</div>
				<div class="col-lg-6 col-xl-4 mb-4">
				<div class="info">
					<p><b><span>Phone &nbsp&nbsp&nbsp&nbsp&nbsp:</span></b> &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp+34 56672 9984 22</a></p>
					<p><b><span>Email &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp:</span></b> <a href="mailto:info@ravoshack.com"> &nbsp&nbsp&nbsp&nbsp&nbsp&nbspinfo@ravoshack.com</a></p>
					<p><b><span>Website &nbsp:</span></b> <a href="http://ravoshack.com/"> &nbsp&nbsp&nbsp&nbsp&nbsp&nbspravoshack.com</a></p>
				  </div>
				</div>				
	        </div>
	    	</div>
	    </section>
		</div>
	</div>

  
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="js/jquery.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/jquery.waypoints.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>
  <script src="js/jquery.animateNumber.min.js"></script>
  <script src="js/scrollax.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="js/google-map.js"></script>
  <script src="js/main.js"></script>
    
  </body>
</html>