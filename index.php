<?php
include('include/top.php');
?>
		<div id="colorlib-main">
			<section class="ftco-section ftco-no-pt ftco-no-pb bg-light">
	    	<div class="container px-0">
	    		<div class="row no-gutters">
	    			<div class="col-md-8 d-flex">
							<div class="row">
								<div class="col-md-12">
									<div class="blog-entry ftco-animate d-md-flex align-items-center">
										<a href="videos/cyber1.mp4" class="img img-2 popup-vimeo d-flex align-items-center justify-content-center" style="background-image: url(images/image_20.jpg);">
											<div class="icon d-flex align-items-center justify-content-center">
												<span class="ion-ios-play"></span>
											</div>
										</a>
										<div class="text text-2 p-4">
				              <h3 class="mb-2"><a href="blog.php" target=" ">Cyber Security</a></h3>
				              <div class="meta-wrap">
												<p class="meta">
				              		<span><i class="icon-calendar mr-2"></i>Sept. 10, 2019</span>
				              	</p>
			              	</div>
				              <p class="mb-4">Cyber security refers to the body of technologies, processes, and practices designed to protect networks, devices, programs, and data from attack, damage, or unauthorized access...</p>
				              <p><a href="blog.php" target=" " class="btn-custom">Read More <span class="ion-ios-arrow-forward"></span></a></p>
				            </div>
									</div>
								</div>
								<div class="col-md-12">
									<div class="blog-entry ftco-animate d-md-flex align-items-center">
										<a href="cscd.php" target=" " class="img img-2 order-md-last" style="background-image: url(images/cybersecurity3.jpg);"></a>
										<div class="text text-2 text-md-right p-4">
				              <h3 class="mb-2"><a href="cscd.php" target=" ">Cyber Security of Consumer Devices</a></h3>
				              <div class="meta-wrap">
												<p class="meta">
				              		<span><i class="icon-calendar mr-2"></i>Sept. 10, 2019</span>				              	
				              	</p>
			              	</div>
				              <p class="mb-4">Weaknesses in the cyber security of internet-connected consumer devices can undermine the privacy and safety of individual users and can be used for large-scale cyber-attacks. This briefing looks at the cyber threats associated with consumer devices and their causes, as well as initiatives to improve device security, and the related challenges.</p>
				              <p><a href="cscd.php" target=" " class="btn-custom">Read More <span class="ion-ios-arrow-forward"></span></a></p>
				            </div>
									</div>
								</div>
							</div>
						</div>

						<div class="col-md-4 d-flex">
							<div class="blog-entry ftco-animate">
								<a href="securitytip.php" target=" " class="img" style="background-image: url(images/cybersecurity2.jpg);"></a>
								<div class="text p-4">
		              <h3 class="mb-2"><a href="securitytip.php" target=" ">Security Tip (ST04-001)</a></h3>
		              <div class="meta-wrap">
										<p class="meta">
		              		<span><i class="icon-calendar mr-2"></i>Sept. 10, 2019</span>		              		
		              	</p>
	              	</div>
		              <p class="mb-4">Cybersecurity is the art of protecting networks, devices, and data from unauthorized access or criminal use and the practice of ensuring confidentiality, integrity, and availability of information.</p>
		              <p><a href="securitytip.php" target=" " class="btn-custom">Read More <span class="ion-ios-arrow-forward"></span></a></p>
		            </div>
							</div>
						</div>
						<div class="col-md-4 d-flex">
							<div class="blog-entry ftco-animate">
								<a href="csia.php" target=" " class="img" style="background-image: url(images/cybersecurity4.jpg);"></a>
								<div class="text p-4">
		              <h3 class="mb-2"><a href="csia.php" target=" ">Cyber Security in Industrial Automation</a></h3>
		              <div class="meta-wrap">
										<p class="meta">
		              		<span><i class="icon-calendar mr-2"></i>Sept. 10, 2019</span>		              		
		              	</p>
	              	</div>
		              <p class="mb-4">Industrial automation and control systems (IACS) used in manufacturing and process automation are increasingly implemented within internal and external company networks. Increased data transfer between systems and networks increases susceptibility for security threats.</p>
		              <p><a href="csia.php" target=" " class="btn-custom">Read More <span class="ion-ios-arrow-forward"></span></a></p>
		            </div>
							</div>
						</div>
						<div class="col-md-4 d-flex">
							<div class="blog-entry ftco-animate">
								<a href="securityposture.php" target=" " class="img" style="background-image: url(images/cybersecurity6.jpg);"></a>
								<div class="text p-4">
		              <h3 class="mb-2"><a href="securityposture.php" target=" ">Improve your security posture</a></h3>
		              <div class="meta-wrap">
										<p class="meta">
		              		<span><i class="icon-calendar mr-2"></i>Sept. 10, 2019</span>		              		
		              	</p>
	              	</div>
		              <p class="mb-4">Billions of records were exposed through thousands of data breaches in 2019. Ransomware continues to loom large as a threat, with cybercriminals adopting ever more sophisticated approaches to attack. Regulatory compliance has proven challenging for corporations and government organizations alike.</p>
		              <p><a href="securityposture.php" target=" " class="btn-custom">Read More <span class="ion-ios-arrow-forward"></span></a></p>
		            </div>
							</div>
						</div>
						<div class="col-md-4 d-flex">
							<div class="blog-entry ftco-animate">
								<a href="securityinfo.php" target=" " class="img" style="background-image: url(images/cybersecurity7.jpg);"></a>
								<div class="text p-4">
		              <h3 class="mb-2"><a href="securityinfo.php" target=" ">Cybersecurity and Information Resilience</a></h3>
		            <div class="meta-wrap">
										<p class="meta">
		              		<span><i class="icon-calendar mr-2"></i>Sept. 10, 2019</span>
		              	</p>
	              	</div>
		              <p class="mb-4">Information Resilience, a domain of Organizational Resilience, empowers organizations to safeguard its information – physical, digital and intellectual property – throughout its lifecycle from source to destruction. This requires the adoption of information security-minded practices that allow stakeholders to gather, store, access and use information securely and effectively.</p>
		              <p><a href="securityinfo.php" target=" " class="btn-custom">Read More <span class="ion-ios-arrow-forward"></span></a></p>
								</div>
							</div>
						</div>																							
	    		</div>
	    	</div>
	    </section>
		</div><!-- END colorlib-MAIN -->
	</div><!-- END colorlib-PAGE -->

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="js/jquery.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/jquery.waypoints.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>
  <script src="js/jquery.animateNumber.min.js"></script>
  <script src="js/scrollax.min.js"></script>  
  <script src="js/google-map.js"></script>
  <script src="js/main.js"></script>
    
  </body>
</html>